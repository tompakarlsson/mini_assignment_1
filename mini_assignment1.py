"""mini assignment - write a custom decorator

Write a python library that implements a decorator. The decorator should log a
line to a file before and after each execution of our decorated functions. The
log line should contain the name of the decorated function (i.e. "Running
function X"/"Done running function X"). The decorator should keep the name and
docstring of the original functions. The decorator should take the name of the
logfile as an argument, example:

    # Logs calls to my_fn to log1.log
    @my_decorator("log1.log")
    def my_fn():
        print("a")

The decorator should work irrelevant of how many arguments the target function
takes, and it should return the same return value as the decorated function."""

from decorators import log


@log(filename='log1.log')
def my_fn(*args):
    for i in args:
        print(i)


my_fn('a', 5, 9.1, True)
