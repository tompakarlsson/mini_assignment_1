# Mini assignment - write a custom decorator

Write a python library that implements a decorator. The decorator should log a
line to a file before and after each execution of our decorated functions. The
log line should contain the name of the decorated function (i.e. "Running
function X"/"Done running function X"). The decorator should keep the name and
docstring of the original functions. The decorator should take the name of the
logfile as an argument, example:

    # Logs calls to my_fn to log1.log
    @my_decorator("log1.log")
    def my_fn():
        print("a")

The decorator should work irrelevant of how many arguments the target function
takes, and it should return the same return value as the decorated function.

## Usage:
The decorator `log` takes one argument <filename> which is the name of the log file to be written to.  
 
    @log('logfile.log')

The function `my_fn()` takes how many arguments as you want and prints them on screen.  
When the `@log` decorator is applied, text is written to a log file and the decorated function still does what it is supposed to do.  
