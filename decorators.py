"""decorators"""

from functools import wraps


def log(filename):
    def decorator_log(func):
        @wraps(func)
        def wrapper_log(*args, **kwargs):
            with open(file=filename, mode='a') as logfile:
                logfile.write(f'Running function "{func.__name__}"\n')
                value = func(*args, **kwargs)
                # The following two lines will also log all args to logfile:
                # for x in args:
                #     logfile.write(f'{str(x)}\n')
                logfile.write(f'Done running function "{func.__name__}"\n')
            return value
        return wrapper_log
    return decorator_log
